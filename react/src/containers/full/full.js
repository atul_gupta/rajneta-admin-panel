import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import { Container } from 'reactstrap';

// IMPORTING ALL COMPONENTS OF THIS PAGE
import Header from '../../components/header/header';
import Sidebar from '../../components/sidebar/sidebar';
import Breadcrumb from '../../components/breadcrumb/breadcrumb';
// VIEWS
import Dashboard from '../../views/dashboard/dashboard';
import ViewPolls from '../../views/polls/view_polls/view_polls';
import CreatePoll from '../../views/polls/create_poll/create_poll';
import Profile from '../../views/profile/profile';
import Journey from '../../views/journey/journey';
import ViewComplaints from '../../views/complaints/view_complaints/view_complaints';
import ComplaintDetails from '../../views/complaints/complaints_details/complaint_details';
import SocialLinks from '../../views/social_links/social_links';
import ContactDetails from '../../views/contact/contact_details';
import ViewMeetings from '../../views/schedule/view_meetings/view_meetings';
import AddMeeting from '../../views/schedule/add_meeting/add_meeting';
import BulkSms from '../../views/BulkSms';
import Payments from '../../views/Payments';
import Users from '../../views/Users/Users';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
            <div className="app-body">
                <Sidebar {...this.props} />
                <main className="main">
                    <Container fluid>
                        <Breadcrumb />
                        <Switch>
                            <Route path="/dashboard" name="Dashboard" component={Dashboard} />
                            <Route path="/view_polls" name="View Polls" component={ViewPolls} />
                            <Route path="/create_poll" name="Create Poll" component={CreatePoll} />
                            <Route path="/profile" name="Profile" component={Profile} />
                            <Route path="/journey" name="Journey" component={Journey} />
                            <Route path="/view_complaints" name="View Complaints" component={ViewComplaints} />
                            <Route path="/complaint_details" name="Complaint Deails" component={ComplaintDetails} />
                            <Route path="/contact_details" name="Contact Details" component={ContactDetails} />
                            <Route path="/social_links" name="Social Links" component={SocialLinks} />
                            <Route path="/view_meetings" name="Social Links" component={ViewMeetings} />
                            <Route path="/add_meeting" name="Social Links" component={AddMeeting} />
                            <Route path="/bulk-sms" name="Bulk Sms" component={BulkSms} />
                            <Route path="/Users" name="Users" component={Users} />
                            <Route path="/payments-details" name="Payments" component={Payments} />
                            
                        </Switch>
                    </Container>
                </main>
            </div>
      </div>
    );
  }
}

export default Full;
