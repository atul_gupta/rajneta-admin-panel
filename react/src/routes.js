const routes = {
  '/': 'Dashboard',
  '/view_polls': 'View Polls',
  '/create_poll': 'Create Poll',
  '/profile': 'Profile',
  '/journey': 'Journey',
  '/languages': 'Languages',
  '/view_complaints': 'Complaints',
  '/complaint_details': 'Complaint Details',
  '/contact_details': 'Contact Details',
  '/social_links': 'Social Links',
  '/view_meetings': 'View Meetings',
  '/add_meeting': 'Add Meeting',
  '/bulk-sms': 'Bulk Sms',
  '/payments-details': 'Payments',
  '/Users': 'Users'
};
export default routes;
