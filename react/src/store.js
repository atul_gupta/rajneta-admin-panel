import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import rootReducer from './redux/rootReducer'
import { logger } from 'redux-logger'
import config from './config'

const initialState = {};

const axiosInstance = axios.create({
    baseURL: `${config.serverUrl}:${config.port}/node/api`
});

// const middleware = [thunk, logger];
const devTools = window.devToolsExtension ? window.devToolsExtension() : f => f;
const enhancers = compose(applyMiddleware(thunk.withExtraArgument(axiosInstance), logger), devTools)
const store = createStore(rootReducer, enhancers);

export default store;
