import {
    FETCH_SCHEDULES,
    FETCH_SCHEDULES_SUCCESS,
    FETCH_SCHEDULES_FAILED,
} from './actionType';

function fetchingSchedules() {
    return {
        type: FETCH_SCHEDULES,
    }
}

function fetchingSchedulesSuccess(payload) {
    return {
        type: FETCH_SCHEDULES_SUCCESS,
        payload
    }
}

function fetchingSchedulesFailed(payload) {
    return {
        type: FETCH_SCHEDULES_FAILED,
        payload
    }
}

export const fetchSchedules = () => (dispatch, getstate, api) => {
    api.get(`/users/fetchMeeting`).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchingSchedulesSuccess(res.data.data))
        }
    })
}

export const addSchedule = data => (dispatch, getstate, api) => {
    api.post(`/users/meeting`, data).then((res) => {
        if (res.data.success === true) {
            alert("Schedule Added Successfully")
        }
    })
}
