import {
    FETCH_SCHEDULES,
    FETCH_SCHEDULES_SUCCESS,
    FETCH_SCHEDULES_FAILED,
} from "./actionType";

const initialState = {
    schedulesList: [],
}

export default function SchedulesReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_SCHEDULES_SUCCESS:
            return {
                ...state, schedulesList: action.payload
            }
        default:
            return state;
    }
}