import { combineReducers } from 'redux';
import register from './registration/reducer';
import login from './Auth/reducer';
import profile from './profile/reducer';
import user from './users/reducer';
import complaints from './complaints/reducer'
import schedule from './schedule/reducer';
import social from './social/reducer';
import journey from './journey/reducer';

export default combineReducers({
    register,
    login,
    profile,
    user,
    complaints,
    schedule,
    social,
    journey
})