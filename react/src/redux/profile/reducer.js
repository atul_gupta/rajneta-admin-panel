import { SAVEPROFILEDATA, GET_IMAGE } from './actionType'

const initialState = {
    profileData: null,
    image: null
};

export default function ProfileReducer(state = initialState, action) {
    switch (action.type) {
        case SAVEPROFILEDATA:
            return {
                ...state,
                profileData: action.payload.data,
            };
        case GET_IMAGE:
            return {
                ...state,
                image: action.payload,
            };
        default:
            return state;
    }
}