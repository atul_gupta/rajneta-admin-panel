import { SAVEPROFILEDATA, GET_IMAGE } from './actionType';


export function saveProfileData(payload) {
    return {
        type: SAVEPROFILEDATA,
        payload
    }
}

export function getImages(payload) {
    return {
        type: GET_IMAGE,
        payload
    }
}

export const saveProfileDetails = data => (dispatch, getState, api) => {
    
    const Images = getState().profile.image;
    const _id = getState().login.profile._id
    let form = new FormData;
    form.append('name', data.name)
    form.append('dateOfBirth', data.dob)
    form.append('about', data.about_yourself)
    form.append('qualification', data.qualification)
    form.append('key', "adminProfile")
    form.append('image', Images.Image)
    
    api.post(`/users/adminProfile`, form).then((res) => {
        console.log('====================================');
        console.log(res);
        console.log('====================================');
        if (res.data.success === true) {
            console.log('====================================');
            console.log(res);
            console.log('====================================');
            dispatch(saveProfileData(res.data.success))
            alert('profile saved')
        }
        else {
            dispatch(saveProfileData(res.data.success))
        }
    }).catch((err) => {
        console.log(err);
    });
}
