import { SAVECONTACTDATA } from './actionType'

const initialState = {
    profileData: null,
};

export default function ProfileReducer(state = initialState, action) {
    switch (action.type) {
        case SAVECONTACTDATA:
            return {
                ...state,
                profileData: action.payload.data,
            };
        default:
            return state;
    }
}