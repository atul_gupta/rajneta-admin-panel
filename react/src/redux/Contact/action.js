import { SAVECONTACTDATA } from './actionType';


export function saveContactData(payload) {
    return {
        type: SAVECONTACTDATA,
        payload
    }
}


export const saveContactDetails = data => (dispatch, getState, api) => {


    let form = new FormData;
    form.append('address', data.address)
    form.append('email', data.email)
    form.append('landlineNo', data.landlineNo)
    form.append('faxNo', data.faxNo)
    form.append('latitude', data.latitude)
    form.append('longitude', data.longitude)
    form.append('key', "adminContact")
    api.post(`/users/contact`, data).then((res) => {
        if (res.data.success === true) {
            console.log('====================================');
            console.log(res);
            console.log('====================================');
            dispatch(saveContactData(res.data.success))
            alert('Contact saved')
        }
        else {
            dispatch(saveContactData(res.data.success))
        }
    }).catch((err) => {
        console.log(err);
    });
}
