import { SAVEPROFILEDATA, GET_IMAGE } from './actionType';


export function saveProfileData(payload) {
    return {
        type: SAVEPROFILEDATA,
        payload
    }
}

export function getImages(payload) {
    return {
        type: GET_IMAGE,
        payload
    }
}

export const saveJourneyDetails = data => (dispatch, getState, api) => {
    
    const Images = getState().journey.image;
    let form = new FormData;
    form.append('journey', data.journey)
    form.append('key', data.key)
    form.append('image', Images.Image)
    
    
    api.post(`/users/journey`, form).then((res) => {
        console.log('====================================');
        console.log(res);
        console.log('====================================');
        if (res.data.success === true) {
            console.log('====================================');
            console.log(res);
            console.log('====================================');
            dispatch(saveProfileData(res.data.success))
            alert('journey saved')
        }
        else {
            dispatch(saveProfileData(res.data.success))
        }
    }).catch((err) => {
        console.log(err);
    });
}
