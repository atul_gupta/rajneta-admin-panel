import {
    FETCH_USERS_LIST,
    FETCH_USERS_LIST_SUCCESS,
    FETCH_USERS_LIST_FAILED,
} from './actionType';

function fetchingUsersList() {
    return {
        type: FETCH_USERS_LIST,
    }
}

function fetchingUsersListSuccess(payload) {
    return {
        type: FETCH_USERS_LIST_SUCCESS,
        payload
    }
}

function fetchingUsersListFailed(payload) {
    return {
        type: FETCH_USERS_LIST_FAILED,
        payload
    }
}

export const fetchUsers = data => (dispatch, getstate, api) => {
    api.get(`/users/fetchuser`).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchingUsersListSuccess(res.data.data))
        }
    })
}