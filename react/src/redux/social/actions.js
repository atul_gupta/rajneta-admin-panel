import {
    FETCH_SOCIAL_LINKS,
    FETCH_SOCIAL_LINKS_SUCCESS,
    FETCH_SOCIAL_LINKS_FAILED,
} from './actionType';

function fetchingSocialLinks() {
    return {
        type: FETCH_SOCIAL_LINKS,
    }
}

function fetchingSocialLinksSuccess(payload) {
    return {
        type: FETCH_SOCIAL_LINKS_SUCCESS,
        payload
    }
}

function fetchingSocialLinksFailed(payload) {
    return {
        type: FETCH_SOCIAL_LINKS_FAILED,
        payload
    }
}

export const fetchSocialLinks = () => (dispatch, getstate, api) => {
    api.get(`/users/getsocialLink`).then((res) => {
        if (res.data.success === true) {
            dispatch(fetchingSocialLinksSuccess(res.data.data))
        }
    })
}

export const editSocialLinks = data => (dispatch, getstate, api) => {
    api.post(`/users/socialLink`, data).then((res) => {
        alert(res.data.message)
        if (res.data.success === true) {
            dispatch(fetchSocialLinks())
        }
    })
}
