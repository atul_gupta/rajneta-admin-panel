import {
    FETCH_SOCIAL_LINKS,
    FETCH_SOCIAL_LINKS_SUCCESS,
    FETCH_SOCIAL_LINKS_FAILED,
} from "./actionType";

const initialState = {
    socialLinks: [],
}

export default function SchedulesReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_SOCIAL_LINKS_SUCCESS:
            return {
                ...state, socialLinks: action.payload
            }
        default:
            return state;
    }
}