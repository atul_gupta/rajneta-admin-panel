import Cookies from 'js-cookie';
import { LOGIN_USER_SUCCESS, LOGIN_USER_FAILED, LOGIN_USER } from './actionType';

export function loginSuccess(payload) {
    return {
        type: LOGIN_USER_SUCCESS,
        payload
    }
}


export function loginFailed(payload) {
    return {
        type: LOGIN_USER_FAILED,
        payload
    }
}

export function loggingInUser(payload) {
    return {
        type: LOGIN_USER,
        payload
    }
}

export const loginUser = userCredentials => {
    return (dispatch, getState, api) =>
        api.post(`/auth/login`, userCredentials).then((res) => {
            Cookies.set("token", res.jwtAccessToken);
            if (res.data.success === true) {
                dispatch(loginSuccess(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};