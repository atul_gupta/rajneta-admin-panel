import { LOGIN_USER_SUCCESS } from './actionType'

const initialState = {
    profile: null,
    token: null,
};

export default function GlobalReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                profile: action.payload.data, 
                token: action.payload.jwtAccessToken

            };
        default:
            return state;
    }
}