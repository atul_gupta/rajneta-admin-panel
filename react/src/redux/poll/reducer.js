import { SAVEPOLLDATA } from './actionType'

const initialState = {
    pollData: null,
};

export default function ProfileReducer(state = initialState, action) {
    switch (action.type) {
        case SAVEPOLLDATA:
            return {
                ...state,
                profileData: action.payload.data, 
            };
        default:
            return state;
    }
}