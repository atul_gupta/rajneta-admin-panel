import { SAVEPOLLDATA } from './actionType';


export function savePollData(payload) {
    return {
        type: SAVEPOLLDATA,
        payload
    }
}

export const pollData = pollData => {
    return (dispatch, getState, api) =>
        api.post(`/users/poll`, pollData).then((res) => {
            if (res.data.success === true) {
                dispatch(savePollData(res.data))
            }
        }).catch((err) => {
            console.log(err);
        });
};