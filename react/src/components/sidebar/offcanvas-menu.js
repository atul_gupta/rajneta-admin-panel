export default {
    items: [
      {
        title: true,
        name: 'main navigation',
        wrapper: {            // optional wrapper object
          element: '',        // required valid HTML5 element tag
          attributes: {}        // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
        },
        class: ''             // optional class names space delimited list for title item ex: "text-center"
      },
      {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-speedometer',
        badge: {
          variant: 'info',
          text: 'NEW'
        }
      },
      {
        name: 'Poll',
        url: '#',
        icon: 'icon-puzzle',
        children: [
          {
            name: 'View Polls',
            url: '/view_polls',
            icon: 'icon-puzzle'
          },
          {
            name: 'Create Polls',
            url: '/create_poll',
            icon: 'icon-puzzle'
          }
        ]
      },
      {
        name: 'Profile',
        url: '/profile',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Journey',
        url: '/journey',
        icon: 'icon-pie-chart'
      },
      // {
      //   name: 'Languages',
      //   url: '/languages',
      //   icon: 'icon-pie-chart'
      // },
      {
        name: 'Complaints',
        url: '/view_complaints',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Contact Details',
        url: '/contact_details',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Social Links',
        url: '/social_links',
        icon: 'icon-calculator'
      },
      {
        name: 'Schedule',
        url: '#',
        icon: 'icon-star',
        children: [
          {
            name: 'View Meetings',
            url: '/view_meetings',
            icon: 'icon-star'
          },
          {
            name: 'Add Meeting',
            url: '/add_meeting',
            icon: 'icon-star'
          }
        ]
      },
      {
        name: 'Bulk Sms',
        url: '/bulk-sms',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Payments',
        url: '/payments-details',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Users',
        url: '/Users',
        icon: 'icon-pie-chart'
      }
    ]
  };
  