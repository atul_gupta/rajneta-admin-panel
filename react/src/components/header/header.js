import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { withRouter } from 'react-router-dom';

import Cookies from 'js-cookie';

class Header extends Component {
    constructor(props) {
        super(props);
    }
    asideToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('aside-menu-hidden');
    }

    logOutUser() {
        Cookies.remove('token');
        this.props.history.push("/login");
    }
    render() {
        return (
            <header className="top-navheader">
                <Navbar color="purple" expand="md">
                    <Nav className="nav-toggle" navbar>
                        <NavItem>
                            <NavLink href="#" onClick={this.asideToggle}><i className="material-icons">menu</i></NavLink>
                        </NavItem>
                    </Nav>
                    <NavbarBrand href="/">
                        <img src="assets/images/logo.png" alt="Logo" />
                    </NavbarBrand>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="#"><i className="material-icons">search</i></NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav>
                                <i className="material-icons">notifications</i>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>Option 1</DropdownItem>
                                <DropdownItem>Option 2</DropdownItem>
                                <DropdownItem>Option 1</DropdownItem>
                                <DropdownItem>Option 2</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav>
                                <img src="assets/images/avatar.png" className="user-avatar" alt="User Avatar" />
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>Profile</DropdownItem>
                                <DropdownItem>Timeline</DropdownItem>
                                <DropdownItem onClick={() => this.logOutUser()}>Logout</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Navbar>
            </header>
        );
    }
}
export default withRouter(Header);