import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Table, Badge } from 'reactstrap';
import { connect } from 'react-redux';
import moment from 'moment';
import { fetchSchedules } from '../../../redux/schedule/action';
import requireAuth from '../../../components/hoc';


class ViewMeetings extends Component {

    //WARNING! To be deprecated in React v17. Use componentDidMount instead.
    componentWillMount() {
        this.props.onfetchSchedules()
    }
    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Table striped hover bordered responsive>
                            <thead>
                                <tr>
                                    <th>Meeting Venue</th>
                                    <th>Meeting Date</th>
                                    <th>Meeting Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.schedulesList.map((item, index) => (
                                        <tr key={index}>
                                            <td>{item.venueName}</td>
                                            <td>{moment(item.venueDateTime).format("DD MMM YYYY")}</td>
                                            <td>{moment(item.venueDateTime).format("HH:mm a")}</td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </Table>
                    </div>
                </Col>
            </Row>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onfetchSchedules: (payload) => {
            dispatch(fetchSchedules(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        schedulesList: state.schedule.schedulesList
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(ViewMeetings));
