import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import TimePicker from 'rc-time-picker';
import { connect } from 'react-redux'
import { addSchedule } from '../../../redux/schedule/action';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-time-picker/assets/index.css';

import requireAuth from '../../../components/hoc';


class AddMeeting extends Component {
    constructor(props) {
        super(props)
        this.state = {
            venue: "",
            date: null,
            startDate: moment()
        };
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleDateChange(date) {
        this.setState({
            startDate: date,
        });
    }

    handleTimeChange(time) {
        this.setState({
            startDate: time,
        });
    }


    handleSubmit(e) {
        e.preventDefault();
        const data = {
            venueName: this.state.venue,
            venueDateTime: this.state.startDate
        }
        this.props.onaddSchedule(data)
    }

    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Form onSubmit={this.handleSubmit}>
                            <Row>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label for="venue_name">Venue Name</Label>
                                        <Input
                                            type="text"
                                            name="venue_name"
                                            id="venue_name"
                                            placeholder=""
                                            value={this.state.venue}
                                            onChange={(e) => this.setState({ ...this.state, venue: e.target.value })}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label for="venue_date">Venue Date</Label>
                                        <DatePicker
                                            selected={this.state.startDate}
                                            onChange={this.handleDateChange}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label for="venue_time">Venue Time</Label><br />
                                        <TimePicker
                                            showSecond={false}
                                            value={this.state.startDate}
                                            className="xxx"
                                            onChange={this.handleTimeChange}
                                            use12Hours
                                            inputReadOnly
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="text-right">
                                <Col sm="12">
                                    <Button type="submit" color="success" className="text-uppercase">submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onaddSchedule: (payload) => {
            dispatch(addSchedule(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        schedulesList: state.schedule.schedulesList
    }
}

export default connect(null, mapDispatchToProps)(requireAuth(AddMeeting));
