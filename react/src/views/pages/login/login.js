import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { loginUser } from "../../../redux/Auth/action";
import { connect } from 'react-redux';


class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      password: null,
      email: null,
    }
    this.submit = this.submit.bind(this);
  }

  submit() {
    const userData = {
      email: this.state.email,
      password: this.state.password,
    }
    this.props.onLoginUser(userData)
  }

  componentWillReceiveProps(){
    this.props.history.push('/dashboard')
}



  render() {
    return (
      <div className="app-login-register-container">
        <h3>Rajneta.co.in</h3>
        <Form>
          <FormGroup>
            <Label for="email">Email</Label>
            <Input type="email" name="email" id="email" placeholder="Email" value={this.state.email} onChange={(event)=>{this.setState({email:event.target.value})}} />
          </FormGroup>
          <FormGroup>
            <Label for="password">Password</Label>
            <Input type="password" name="password" id="password" placeholder="Password" value={this.state.password} onChange={(event)=>{this.setState({password:event.target.value})}}/>
          </FormGroup>
          <div className="flex-row align-items-center justify-content-between">
            <Button color="success" onClick={() => this.submit()}>Login</Button>
            <Link to="/register" className="pull-right">Create an Account</Link>
          </div>
        </Form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onLoginUser: (payload) => {
      console.log(payload);
      dispatch(loginUser(payload));
    }
  }
}


function mapStateToProps(state) {
  return {
    profileData : state.login.profile
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

