import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { registration } from "../../../redux/registration/action";


class Register extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
        this.submit = this.submit.bind(this);
    }

    submit() {
        const data = {

        }
        this.props.onRegistration(data)
    }


    render() {
        return (
            <div className="app-login-register-container">
                <h3>register</h3>
                <Form>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" name="name" id="name" placeholder="Name" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="email" name="email" id="email" placeholder="Email" />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Password</Label>
                        <Input type="password" name="password" id="password" placeholder="Password" />
                    </FormGroup>
                    <div className="flex-row align-items-center justify-content-between">
                        <Button color="success" onClick={() => this.submit()}>Register</Button>
                        <Link to="/login" className="pull-right">I am already a member</Link>
                    </div>
                </Form>
            </div>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return {
        onRegistration: (payload) => {
            dispatch(registration(payload));
        },
    }
}

function mapStateToProps(state) {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);



