import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button, Badge } from 'reactstrap';
import { connect } from 'react-redux';
import requireAuth from '../../components/hoc';
import { getImages, saveJourneyDetails } from "../../redux/journey/action";
import { Icon } from 'react-icons-kit';
import { trashO } from 'react-icons-kit/fa/trashO'
import { plus } from 'react-icons-kit/fa/plus'

class Journey extends Component {
    constructor(props) {
        super(props)

        this.state = {
            journey_details: null
        }

        this.savePreview = this.savePreview.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    savePreview(preview, Image) {
        const file = {
            preview, Image
        }
        this.props.onGetImages(file);
    }

    _handleImageChange(e) {
        e.preventDefault();
        var fileTypes = ['jpg', 'jpeg', 'png'];
        if (e.target.files[0]) {
            var extension = e.target.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if (isSuccess) {
                let reader = new FileReader();
                let file = e.target.files[0];

                reader.onloadend = () => {
                    this.savePreview(reader.result, file);
                    this.setState({
                        imageurl: reader.result
                    })
                }
                reader.readAsDataURL(file)
            } else {
                alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
            }
        } else {

        }
    }

    onSubmit() {

        const userData = {
            journey: this.state.journey_details,
            key: 'journeyUpdate',
        }
        this.props.onSaveJourneyDetails(userData);

    }
    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Form>
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="journey_details">Enter Your Journey Details</Label>
                                        <Input type="textarea" rows="6" name="journey_details" id="journey_details" placeholder="Enter Your Journey Details" value={this.state.journey_details}
                                            onChange={(e) => this.setState({ ...this.state, journey_details: e.target.value })} />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="rpofile_pic">Picture</Label>
                                        {
                                            this.props.image === null ?
                                                <div>
                                                    <label for="image123" type="file" name="file" id="exampleFile" className="plus formcontrol">
                                                        <Badge className="striped_border">
                                                            <Icon icon={plus} trashO className="v-align-middle padded" style={{ color: '#a6a6a6' }} />
                                                        </Badge>
                                                    </label>
                                                    <input type="file" name="myfile" onChange={(e) => this._handleImageChange(e)} id="image123" style={{ display: 'none' }} />
                                                </div>
                                                :
                                                <div className="mr-3 mb-3 hvrbox"  >
                                                    <img src={this.props.image.preview} style={{ height: '100px' }} />
                                                    <div class="hvrbox-layer_top">
                                                        <div class="hvrbox-text">
                                                            <Icon icon={trashO} size={40} className="v-align-middle" style={{ color: '#fff' }} />
                                                        </div>
                                                    </div>
                                                </div>
                                        }

                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="text-right">
                                <Col sm="12">
                                    <Button color="success" className="text-uppercase" onClick={() => this.onSubmit()}>submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }
}



function mapDispatchToProps(dispatch) {
    return {
        onGetImages: (payload) => {
            dispatch(getImages(payload));
        },
        onSaveJourneyDetails: (payload) => {
            dispatch(saveJourneyDetails(payload));
        },
    }
}

function mapStateToProps(state) {
    return {
        image: state.journey.image,


    }
}

export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(Journey));

