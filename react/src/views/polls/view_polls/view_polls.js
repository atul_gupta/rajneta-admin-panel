import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { Row, Col } from 'reactstrap';

import requireAuth from '../../../components/hoc';

const data = {
	labels: [
		'Option A',
		'Option B',
        'Option C',
        'Option D'
	],
	datasets: [{
		data: [300, 50, 100, 200],
		backgroundColor: [
		'#FF6384',
		'#36A2EB',
        '#FFCE56',
        '#3f51b5'
		],
		hoverBackgroundColor: [
		'#FF6384',
		'#36A2EB',
        '#FFCE56',
        '#3f51b5'
		]
	}]
};

class ViewPolls extends Component {
    render() {
        return (
            <Row>
                <Col sm="6">
                    <div className="card-box">
                        <h3 className="card-title">Latest Poll Resuts</h3>
                        <p><span className="font-500">Q.</span> What is wrong with each of these methods</p>
                        <div className="card-container">
                            
                            <Doughnut data={data} />
                        </div>
                    </div>
                </Col>
                <Col sm="6">
                    <div className="card-box">
                        <h3 className="card-title">Latest Poll Resuts</h3>
                        <p><span className="font-500">Q.</span> What is wrong with each of these methods</p>
                        <div className="card-container">
                            
                            <Doughnut data={data} />
                        </div>
                    </div>
                </Col>
                <Col sm="6">
                    <div className="card-box">
                        <h3 className="card-title">Latest Poll Resuts</h3>
                        <p><span className="font-500">Q.</span> What is wrong with each of these methods</p>
                        <div className="card-container">
                            
                            <Doughnut data={data} />
                        </div>
                    </div>
                </Col>
                <Col sm="6">
                    <div className="card-box">
                        <h3 className="card-title">Latest Poll Resuts</h3>
                        <p><span className="font-500">Q.</span> What is wrong with each of these methods</p>
                        <div className="card-container">
                            
                            <Doughnut data={data} />
                        </div>
                    </div>
                </Col>
                <Col sm="6">
                    <div className="card-box">
                        <h3 className="card-title">Latest Poll Resuts</h3>
                        <p><span className="font-500">Q.</span> What is wrong with each of these methods</p>
                        <div className="card-container">
                            
                            <Doughnut data={data} />
                        </div>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default requireAuth(ViewPolls);
