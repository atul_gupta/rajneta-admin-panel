import React, { Component } from 'react';
import { Row, Col, Table, Badge, Form, Label, Input, Button } from 'reactstrap';
import { connect } from 'react-redux'
import { fetchUsers } from '../../redux/users/action'
import requireAuth from '../../components/hoc';

class Users extends Component {

    //WARNING! To be deprecated in React v17. Use componentDidMount instead.
    componentWillMount() {
        this.props.onfetchUsers()
    }
    render() {
        return (
            <div>
                <Row className="table">
                    <Col sm="12">
                        <div className="card-box">
                            <Table striped hover bordered responsive>
                                <thead>
                                    <tr >
                                        <th>S.NO</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile No.</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {
                                        this.props.usersList.map((user, index) => (
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{user.name}</td>
                                                <td>{user.email}</td>
                                                <td>{user.phone}</td>
                                            </tr>
                                        ))
                                    }
                                </tbody>
                            </Table>
                        </div>
                    </Col>
                </Row>
            </div>

        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onfetchUsers: (payload) => {
            dispatch(fetchUsers(payload));
        }
    }
}


function mapStateToProps(state) {
    return {
        usersList: state.user.usersList
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(Users));
