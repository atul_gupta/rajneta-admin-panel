import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { saveContactDetails } from "../../redux/Contact/action";

import requireAuth from '../../components/hoc';

class ContactDetails extends Component {
    constructor(props) {
        super(props)

        this.state = {
            address: null,
            email: null,
            landlineNo: null,
            faxNo: null,
            latitude: null,
            longitude: null
        }
        this.onSubmit = this.onSubmit.bind(this);
    }
    onSubmit() {

        const userData = {
            address: this.state.address,
            email: this.state.email,
            landlineNo: this.state.landlineNo,
            faxNo: this.state.faxNo,
            latitude: this.state.latitude,
            longitude: this.state.longitude
            
        }
        this.props.onSaveContactDetails(userData);

    }
    render() {
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Form>
                            <Row>
                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="address">Enter Your Address</Label>
                                        <Input type="textarea" name="address" id="address" placeholder="Enter Your Address"
                                            onChange={(e) => this.setState({ ...this.state, address: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label for="email">Enter Your Email</Label>
                                        <Input type="email" name="email" id="email" placeholder="Enter Your Email"
                                            onChange={(e) => this.setState({ ...this.state, email: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label for="landline_no">Enter Landline No.</Label>
                                        <Input type="number" name="landline_no" id="landline_no" placeholder="Enter Landline No."
                                            onChange={(e) => this.setState({ ...this.state, landlineNo: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="4">
                                    <FormGroup>
                                        <Label for="fax_no">Enter Fax No.</Label>
                                        <Input type="number" name="fax_no" id="fax_no" placeholder="Enter Fax No."
                                            onChange={(e) => this.setState({ ...this.state, faxNo: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="contact_latitude">Enter Latitude For Google Map</Label>
                                        <Input type="text" name="contact_latitude" id="contact_latitude" placeholder="Enter Latitude For Google Map"
                                            onChange={(e) => this.setState({ ...this.state, latitude: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="contact_longitude">Enter Longitude For Google Map</Label>
                                        <Input type="text" name="contact_longitude" id="contact_longitude" placeholder="Enter longitude For Google Map"
                                            onChange={(e) => this.setState({ ...this.state, longitude: e.target.value })} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="text-right">
                                <Col sm="12">
                                    <Button color="success" className="text-uppercase" onClick={() => this.onSubmit()}>submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onSaveContactDetails: (payload) => {
            dispatch(saveContactDetails(payload));
        },
    }
}

function mapStateToProps(state) {
    return {
        image: state,


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(ContactDetails));
