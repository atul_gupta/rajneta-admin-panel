import React, { Component } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { Row, Col, Table, Badge } from 'reactstrap';
import requireAuth from '../../components/hoc';

const data = {
    labels: [
        'Option A',
        'Option B',
        'Option C',
        'Option D'
    ],
    datasets: [{
        data: [300, 50, 100, 200],
        backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56',
            '#3f51b5'
        ],
        hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56',
            '#3f51b5'
        ]
    }]
};

class Dashboard extends Component {

    render() {

        return (
            <Row>
                <Col sm="6" className="card-box" style={{ marginLeft: "18px" }}>
                    <div className="table-responsive">
                        <h3 className="card-title">Latest Poll Resuts</h3>
                        <p><span className="font-500">Q.</span> What is wrong with each of these methods</p>
                        <div className="card-container">

                            <Doughnut data={data} />
                        </div>
                    </div>
                </Col>
                <Col sm="5" className="card-box" style={{ marginLeft: "50px" }}>
                    <div className="table-responsive" >
                        <h3 className="card-title">Compaint box</h3>
                        <div className="card-container">
                            <Table>
                                <tbody>
                                    <tr>
                                        <th>Total Complaints</th>
                                        <td><Badge color="dark" className="badge-block">100</Badge></td>
                                    </tr>
                                    <tr>
                                        <th>Today's Complaints</th>
                                        <td><Badge color="warning" className="badge-block">10</Badge></td>
                                    </tr>
                                    <tr>
                                        <th>Solved Complaints</th>
                                        <td><Badge color="success" className="badge-block">5</Badge></td>
                                    </tr>
                                    <tr>
                                        <th>Unsolved Complaints</th>
                                        <td><Badge color="danger" className="badge-block">25</Badge></td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </div>
                </Col>
            </Row>
        );
    }
}

export default requireAuth(Dashboard);
