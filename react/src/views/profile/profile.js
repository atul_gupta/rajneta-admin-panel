import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input, Button, Badge } from 'reactstrap';
import { Icon } from 'react-icons-kit';
import { trashO } from 'react-icons-kit/fa/trashO'
import { plus } from 'react-icons-kit/fa/plus'
import { connect } from 'react-redux';
import requireAuth from '../../components/hoc';
import { getImages, saveProfileDetails } from "../../redux/profile/action";

class Profile extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: null,
            dob: null,
            about_yourself: null,
            qualification: null,
            imageurl: null
        }
        this.savePreview = this.savePreview.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    

    savePreview(preview, Image) {
        const file = {
            preview, Image
        }
        this.props.onGetImages(file);
    }

    _handleImageChange(e) {
        e.preventDefault();
        var fileTypes = ['jpg', 'jpeg', 'png'];
        if (e.target.files[0]) {
            var extension = e.target.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if (isSuccess) {
                let reader = new FileReader();
                let file = e.target.files[0];

                reader.onloadend = () => {
                    this.savePreview(reader.result, file);
                    this.setState({
                        imageurl: reader.result
                    })
                }
                reader.readAsDataURL(file)
            } else {
                alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
            }
        } else {

        }
    }

    onSubmit() {

        const userData = {
            name: this.state.name,
            dob: this.state.dob,
            about_yourself: this.state.about_yourself,
            qualification: this.state.qualification,
            
        }
        this.props.onSaveProfileDetails(userData);

    }

    render() {        
        return (
            <Row>
                <Col sm="12">
                    <div className="card-box">
                        <Form>
                            <Row>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="name">Name</Label>
                                        <Input type="text" name="name" id="name" placeholder="Enter Your Name" value={this.state.name}
                                            onChange={(e) => this.setState({ ...this.state, name: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="dob">Date Of Birth</Label>
                                        <Input type="text" name="dob" id="dob" placeholder="Enter Your Date Of Birth" value={this.state.dob}
                                            onChange={(e) => this.setState({ ...this.state, dob: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="qualification">Qualification</Label>
                                        <Input type="textarea" name="qualification" id="qualification" placeholder="Enter Your Qualification" rows="4"
                                            value={this.state.qualification}
                                            onChange={(e) => this.setState({ ...this.state, qualification: e.target.value })} />
                                    </FormGroup>
                                </Col>
                                <Col sm="6">
                                    <FormGroup>
                                        <Label for="rpofile_pic">Profile Picture</Label>
                                        {
                                            this.props.image === null ?
                                                <div>
                                                    <label for="image123" type="file" name="file" id="exampleFile" className="plus formcontrol">
                                                        <Badge className="striped_border">
                                                            <Icon icon={plus} trashO className="v-align-middle padded" style={{ color: '#a6a6a6' }} />
                                                        </Badge>
                                                    </label>
                                                    <input type="file" name="myfile" onChange={(e) => this._handleImageChange(e)} id="image123" style={{ display: 'none' }} />
                                                </div>
                                                :
                                                <div className="mr-3 mb-3 hvrbox"  >
                                                    <img src={this.props.image.preview} style={{ height: '100px' }} />
                                                    <div class="hvrbox-layer_top">
                                                        <div class="hvrbox-text">
                                                            <Icon icon={trashO} size={40} className="v-align-middle" style={{ color: '#fff' }} />
                                                        </div>
                                                    </div>
                                                </div>
                                        }

                                    </FormGroup>
                                </Col>
                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="about_yourself">About Yourself</Label>
                                        <Input type="textarea" name="about_yourself" id="about_yourself" placeholder="Enter About Yourself" rows="6"
                                            value={this.state.about_yourself}
                                            onChange={(e) => this.setState({ ...this.state, about_yourself: e.target.value })} />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row className="text-right">
                                <Col sm="12">
                                    {/* <Button color="info" className="text-uppercase">Edit Profile</Button> */}
                                    <Button style={{ marginLeft: "15px" }} color="success" className="text-uppercase" onClick={() => this.onSubmit()}>submit</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Col>
            </Row>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onGetImages: (payload) => {
            dispatch(getImages(payload));
        },
        onSaveProfileDetails: (payload) => {
            dispatch(saveProfileDetails(payload));
        },
    }
}

function mapStateToProps(state) {
    return {
        image: state.profile.image,
        

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(requireAuth(Profile));

